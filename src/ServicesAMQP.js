// @flow
import type { Channel, Connection } from 'amqplib';
import type { QueueListener } from './listenRPCQueues';

import createChannel from './createChannel';
import createConnection from './createConnection';
import listenRPCQueues from './listenRPCQueues';

type Options = {
  url: string,
  rpcRoutes: { [string]: Function }
}

class ServicesAMQP {
  connection: Connection;

  options: Options;

  channel: Channel;

  RPCListeners: Array<QueueListener>;

  constructor(options: Options) {
    this.options = options;
  }

  async setup() {
    const { url, rpcRoutes } = this.options;
    const connection = await createConnection(url);

    this.connection = connection;
    this.channel = await createChannel(connection);
    this.RPCListeners = listenRPCQueues(this.channel, rpcRoutes);

    return this;
  }

  async destroy() {
    await this.channel.close();
    await this.connection.close();
  }
}

export default ServicesAMQP;
