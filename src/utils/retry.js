const retry = 3;
const wait = 2000;

type RetryFn = {
  func: Function,
  errorMessage: string,
  retryMessage: string,
}

const retryFn = async ({ func, errorMessage, retryMessage }: RetryFn) => {
  let localRetry = retry;

  const recursive = async () => {
    let result;
    let error;

    try {
      localRetry -= 1;
      result = await func();
    } catch (e) {
      error = e;
      if (localRetry !== 0) {
        console.info(retryMessage);
        await new Promise(r => setTimeout(r, wait));

        return recursive();
      }
    }

    if (!result) {
      console.error(errorMessage);
      console.error(error);
      throw new Error(errorMessage);
    }

    return result;
  };

  const awaitedResult = await recursive();

  return awaitedResult;
};

export default retryFn;
