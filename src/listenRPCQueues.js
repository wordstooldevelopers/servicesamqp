// @flow
import type { Channel, Message } from 'amqplib';
import AMQPContext from './AMQPContext';

type QueueListenerType = {
  channel: Channel,
  queue: string,
  controller: Function,
}

export class QueueListener {
  channel: Channel;

  queue: string;

  controller: (message: AMQPContext) => {} | Promise<AMQPContext>;

  consumerTag: string;

  constructor(options: QueueListenerType) {
    const { channel, queue, controller } = options;
    this.channel = channel;
    this.queue = queue;
    this.controller = controller;
    this.addQueueListener()
      .catch((e) => {
        throw new Error(e);
      });
  }

  async addQueueListener() {
    this.channel.assertQueue(this.queue, { durable: false });
    this.channel.prefetch(1);
    const { consumerTag } = await this.channel.consume(
      this.queue, (msg: ?Message) => this.onReceivedMessage(msg),
    );
    this.consumerTag = consumerTag;
    console.debug('Start listen queue: %s', this.queue);
  }

  async onReceivedMessage(msg: ?Message) {
    if (!msg) {
      console.error('Received null message');
      return null;
    }
    const context = new AMQPContext(msg);
    const { controller, channel } = this;

    try {
      if (controller instanceof Promise) {
        controller(context);
      } else {
        await controller(context);
      }
    } catch (error) {
      return channel.nack(msg);
    }

    return channel.ack(msg);
  }
}

export default (channel: Channel, queuesRoutes: { [string]: Function }): Array<QueueListener> => {
  const queues = Object.keys(queuesRoutes);

  return queues.map(queue => new QueueListener({
    channel, queue, controller: queuesRoutes[queue],
  }));
};
