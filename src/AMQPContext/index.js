// @flow
import type { Message } from 'amqplib';

export default class AMQPContext {
  message: Message;

  request: {
    json: Object | null,
  };

  response: {
    statusCode: number,
    json: Object | null,
  };

  constructor(message: Message) {
    this.message = message;
    this.response = {
      statusCode: 200,
      json: {},
    };

    const contentString = message.content.toString();

    let json;
    try {
      json = JSON.parse(contentString);
    } catch (e) {
      console.error('Failed parse json');
    }

    this.request = {
      json: json || null,
    };
  }

  validationError(errors: Object) {
    this.response.statusCode = 400;
    this.response.json = { errors };
  }

  ok(data: Object) {
    this.response.statusCode = 200;
    this.response.json = { data };
  }
}
