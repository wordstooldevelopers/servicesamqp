import type { Channel, Connection } from 'amqplib';
import retryFn from './utils/retry';

const createChannel = async (connection: Connection): Promise<Channel> => {
  const channel = await retryFn({
    func: (): Promise<Channel> => connection.createChannel(),
    errorMessage: 'Failed create channel to AMQP',
    retryMessage: 'Retry create channel to AMQP',
  });

  console.info('Success created channel to AMQP');

  return channel;
};

export default createChannel;
