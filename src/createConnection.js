import type { Connection } from 'amqplib';
import { connect } from 'amqplib';
import retryFn from './utils/retry';

const createConnection = async (url: string) => {
  const connection = await retryFn({
    func: (): Promise<Connection> => connect(url),
    errorMessage: 'Failed connect to AMQP',
    retryMessage: 'Retry create AMQP connection',
  });

  console.info('Success created connection to AMQP');

  return connection;
};


export default createConnection;
